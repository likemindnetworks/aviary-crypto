'use strict';

var
	_ = require('lodash'),
	crypto = require('crypto-js'),
	r = require('jsrsasign');

module.exports = {
	rsaKeygen: function() {
		return r.KEYUTIL.generateKeypair('RSA', 1024);
	},

	nearestTimeSlot: function(timeSlotSize) {
		return Math.floor(Date.now() / timeSlotSize) * timeSlotSize;
	},

	// prgoressive HMAC
	pHMAC: function(passHash, data) {
		if (!passHash || !data || !data.length) {
			throw new Error('Must supply passHash or data');
		}

		var
			hmac = crypto.algo.HMAC.create(
				crypto.algo.SHA512, passHash
			);

		data.forEach(function(v) {
			hmac.update(v);
		});

		return hmac.finalize();
	},

	// 256 bits PBKDF2 pass hash 500 iterations
	passHash: function(password, salt) {
		if (!salt || !password) {
			throw new Error('Must supply password or salt');
		}

		return crypto.PBKDF2(
			password,
			salt,
			{
				keySize: 256/32,
				iterations: 500
			}
		);
	},

	requestSignature: function(
			passHash, sessionId, pathName, queryString, timeSlotSize
	) {
		return this
			.pHMAC(
				passHash,
				[
					// session id
					sessionId || '',

					// pathname
					pathName || '',
					
					// query string
					queryString || '',

					// then the nearest time slot
					this.nearestTimeSlot(timeSlotSize)
				]
			);
	}
};
